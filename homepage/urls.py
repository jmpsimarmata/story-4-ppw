from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('Johanes_profile1/', views.profile1, name='profile1'),
    path('Johanes_profile2/', views.profile2, name='profile2'),
    path('Johanes_favorite/', views.myfavorite, name='myfavorite'),
    path('Johanes_Social_Media/', views.socialmedia, name='socialmedia'),
    path('navbar/',views.navbar, name='navbar'),
    path('HomeStory1/', views.homeStory1, name='homeStory1'),
    path('AboutMe/', views.aboutme, name='aboutme'),
    path('MyContact/', views.mycontact, name='mycontact')
]