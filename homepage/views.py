from django.shortcuts import render

# Create your views here.
# UNTUK STORY 3
def home(request):
    return render(request, 'home.html')

def profile1(request):
    return render(request, 'profile1.html')

def profile2(request):
    return render(request, 'profile2.html')

def myfavorite(request):
    return render(request, 'myfavorite.html')

def socialmedia(request):
    return render(request, 'socialmedia.html')

def navbar(request):
    return render(request, 'navbar.html')

#UNTUK STORY 1
def homeStory1(request):
    return render(request, 'homeStory1.html')

def aboutme(request):
    return render(request, 'aboutme.html')

def mycontact(request):
    return render(request, 'mycontact.html')
